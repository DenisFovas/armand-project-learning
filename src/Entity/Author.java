package Entity;

public class Author {

    private int id;
    private String name;
    private String country;
    private int numberOfBooks;

    public Author(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.country = builder.country;
        this.numberOfBooks = builder.numberOfBooks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberOfBooks() {
        return numberOfBooks;
    }

    public void setNumberOfBooks(int numberOfBooks) {
        this.numberOfBooks = numberOfBooks;
    }

    public int getId() {
        return id;
    }

    public static class Builder {
        private int id;
        private String name;
        private String country;
        private int numberOfBooks;

        public Builder(int id) {
            this.id = id;
            this.country = null;
            this.name = null;
            this.numberOfBooks = 0;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder setNumberOfBooks(int numberOfBooks) {
            this.numberOfBooks = numberOfBooks;
            return this;
        }

        public Author build() {
            return new Author(this);
        }
    }
}
