package Entity.ConstructionEnumerations;

public enum NotebookEnum implements AbstractEnum {
    TYPE,
    SIZE,
    PRICE,
    QUANTITY
}
