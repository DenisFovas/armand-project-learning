package Entity.ConstructionEnumerations;

public enum AuthorEnum implements AbstractEnum {
    NAME,
    COUNTRY,
    NUMBER_OF_BOOKS
}
