package Entity.ConstructionEnumerations;

public enum BookEnum implements AbstractEnum {
    TITLE,
    SHORT_DESCRIPTION,
    ISBN,
    QUANTITY,
    AUTHOR_ID
}
