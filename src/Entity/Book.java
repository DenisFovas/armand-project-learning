package Entity;

public class Book {
    private int id;
    private String title;
    private String shortDescription;
    private String isbn;
    private Author author;
    private int quantity;

    public Book(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.shortDescription = builder.shortDescription;
        this.isbn = builder.isbn;
        this.author = builder.author;
        this.quantity = builder.quantity;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static class Builder {
        private int id;
        private String title;
        private String shortDescription;
        private String isbn;
        private Author author;
        private int quantity;

        public Builder(int id) {
            this.id = id;
            this.title = null;
            this.shortDescription = null;
            this.isbn = "";
            this.author = null;
            this.quantity = 0;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
            return this;
        }

        public Builder setIsbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public Builder setAuthor(Author author) {
            this.author = author;
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Book build() {
            return new Book(this);
        }
    }
}
