package Entity;

public interface EntityBuilder {
    abstract class Builder {
        abstract Object build();
    }
}
