package Entity;

public class Notebook implements EntityBuilder{
    public static final String[] TYPES = {"MATH", "SIMPLE", "BLANK"};
    public static final String[] SIZES = {"A4", "A5", "A3"};

    private int id;
    private String type;
    private String size;
    private float price;
    private int quantity;

    public Notebook(Builder builder) {
        this.id = builder.id;
        this.type = builder.type;
        this.size = builder.size;
        this.price = builder.price;
        this.quantity = builder.quantity;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static class Builder {
        private int id;
        private String type;
        private String size;
        private float price;
        private int quantity;

        public Builder(int id) {
            this.id = id;
            type = null;
            size = null;
            price = 0;
            quantity = 0;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setSize(String size) {
            this.size = size;
            return this;
        }

        public Builder setPrice(float price) {
            this.price = price;
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Notebook build() {
            return new Notebook(this);
        }
    }
}
