package Repository;

import Entity.Author;
import Entity.ConstructionEnumerations.AbstractEnum;
import Entity.ConstructionEnumerations.AuthorEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AuthorRepository implements IRepository<Author> {
    private List<Author> authors;

    public AuthorRepository() {
        this.authors = new ArrayList<>();
    }

    @Override
    public Author create(int id, Map<AbstractEnum, String> values) {
        String name = null;
        String country = null;
        int numberOfBooks = 0;

        if (values.containsKey(AuthorEnum.NAME)) {
            name = values.get(AuthorEnum.NAME);
        }

        if (values.containsKey(AuthorEnum.COUNTRY)) {
            country = values.get(AuthorEnum.COUNTRY);
        }

        if (values.containsKey(AuthorEnum.NUMBER_OF_BOOKS)) {
            numberOfBooks = Integer.valueOf(values.get(AuthorEnum.NUMBER_OF_BOOKS));
        }

        Author author = new Author.Builder(id)
                .setCountry(country)
                .setName(name)
                .setNumberOfBooks(numberOfBooks)
                .build();

        this.authors.add(author);

        return author;
    }

    @Override
    public Author findOne(int id) {
        Author requiredAuthor = null;

        for (Author author: authors) {
            if (author.getId() == id) {
                requiredAuthor = author;
            }
        }

        return requiredAuthor;
    }

    @Override
    public List<Author> findAll() {
        return this.authors;
    }

    @Override
    public void update(Author oldEntity, Author newEntity) {

    }

    @Override
    public void delete(int id) {
        for (Author author: authors) {
            if (author.getId() == id) {
                authors.remove(author);
            }
        }
    }
}
