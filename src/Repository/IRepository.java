package Repository;

import Entity.ConstructionEnumerations.AbstractEnum;

import java.util.List;
import java.util.Map;

public interface IRepository<K> {
    K create(int id, Map<AbstractEnum, String> values);
    K findOne(int id);
    List<K> findAll();
    void update(K oldEntity, K newEntity);
    void delete(int id);
}
