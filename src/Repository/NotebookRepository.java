package Repository;

import Entity.ConstructionEnumerations.AbstractEnum;
import Entity.ConstructionEnumerations.NotebookEnum;
import Entity.Notebook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NotebookRepository implements IRepository<Notebook> {
    private List<Notebook> notebooks;

    public NotebookRepository() {
        this.notebooks = new ArrayList<>();
    }

    @Override
    public Notebook create(int id, Map<AbstractEnum, String> values) {
        String type = null;
        String size = null;
        float price = 0;
        int quantity = 0;

        if (values.containsKey(NotebookEnum.PRICE)) {
            price = Float.parseFloat(values.get(NotebookEnum.PRICE));
        }

        if (values.containsKey(NotebookEnum.TYPE)) {
            type = values.get(NotebookEnum.TYPE);
        }

        if (values.containsKey(NotebookEnum.SIZE)) {
            size = values.get(NotebookEnum.SIZE);
        }

        if (values.containsKey(NotebookEnum.QUANTITY)) {
            quantity = Integer.parseInt(values.get(NotebookEnum.QUANTITY));
        }

        Notebook notebook = new Notebook.Builder(id)
                .setPrice(price)
                .setType(type)
                .setSize(size)
                .setQuantity(quantity)
                .build();

        notebooks.add(notebook);
        return notebook;
    }

    @Override
    public Notebook findOne(int id) {
        Notebook notebook = null;
        return null;
    }

    @Override
    public List<Notebook> findAll() {
        return this.notebooks;
    }

    @Override
    public void update(Notebook oldEntity, Notebook newEntity) {

    }

    @Override
    public void delete(int id) {
        Notebook notebook = null;

    }
}
