package Repository;

import Entity.Author;
import Entity.Book;
import Entity.ConstructionEnumerations.AbstractEnum;
import Entity.ConstructionEnumerations.BookEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BookRepository implements IRepository<Book>{
    private List<Book> books;

    public BookRepository() {
        this.books = new ArrayList<>();
    }

    @Override
    public Book create(int id, Map<AbstractEnum, String> values) {
        String title = null;
        String shortDescription = null;
        String isbn = null;
        int quantity = 0;
        Author author = null;

        if (values.containsKey(BookEnum.TITLE)) {
            title = values.get(BookEnum.TITLE);
        }

        if (values.containsKey(BookEnum.SHORT_DESCRIPTION)) {
            shortDescription = values.get(BookEnum.SHORT_DESCRIPTION);
        }

        if (values.containsKey(BookEnum.ISBN)) {
            isbn = values.get(BookEnum.ISBN);
        }

        if (values.containsKey(BookEnum.QUANTITY)) {
            quantity = Integer.getInteger(values.get(BookEnum.QUANTITY));
        }

        if (values.containsKey(BookEnum.AUTHOR_ID)) {
            int authorId = Integer.getInteger(values.get(BookEnum.QUANTITY));

            AuthorRepository authorRepository = new AuthorRepository();

            author = authorRepository.findOne(authorId);
        }

        Book b = new Book.Builder(id)
                .setAuthor(author)
                .setIsbn(isbn)
                .setQuantity(quantity)
                .setShortDescription(shortDescription)
                .setTitle(title)
                .build();

        this.books.add(b);

        return b;
    }

    @Override
    public Book findOne(int id) {
        Book requiredBook = null;

        for (Book book :
                books) {
            if (book.getId() == id) {
                requiredBook = book;
            }
        }

        return requiredBook;
    }

    @Override
    public List<Book> findAll() {
        return this.books;
    }

    @Override
    public void update(Book oldEntity, Book newEntity) {

    }

    @Override
    public void delete(int id) {
        for (Book book :
                this.books) {
            if (book.getId() == id) {
                this.books.remove(book);
            }
        }
    }
}
