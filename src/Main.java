import Entity.Author;
import Entity.Book;
import Entity.ConstructionEnumerations.AbstractEnum;
import Entity.ConstructionEnumerations.AuthorEnum;
import Entity.ConstructionEnumerations.BookEnum;
import Repository.AuthorRepository;
import Repository.BookRepository;
import sun.net.www.protocol.http.AuthenticationHeader;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<AbstractEnum, String> bookMap = new HashMap<>();

        bookMap.put(AuthorEnum.NAME, "denis fovas");
        bookMap.put(AuthorEnum.COUNTRY, "RO");

        AuthorRepository authorRepository = new AuthorRepository();

        Author author = authorRepository.create(1, bookMap);

        System.out.println(author.getName());
        System.out.println(author.getCountry());
        System.out.println(author.getId());
        System.out.println(author.getNumberOfBooks());
    }
}
